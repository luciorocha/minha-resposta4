/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import utfpr.ct.dainf.if62c.pratica.*;
        
public class Pratica41 {
    
    public static void main(String [] args){
        
        Circulo c = new Circulo(10);
        Elipse e = new Elipse(10,20);
        System.out.println("Circulo (area): " + c.getArea());
        System.out.println("Circulo (perimetro): " + c.getPerimetro());
        System.out.println("Elipse (area): " + e.getArea());
        System.out.println("Elipse (perimetro): " + e.getPerimetro());
                
    }
    
}
